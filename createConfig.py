setups = {
  "sh" : 
    """ 
    export PYTHONPATH={binDir}/python:$PYTHONPATH
    export LD_LIBRARY_PATH={binDir}:$LD_LIBRARY_PATH
    """,
  "csh" :   
    """ 
    setenv PYTHONPATH {binDir}/python:$PYTHONPATH
    setenv LD_LIBRARY_PATH {binDir}:$LD_LIBRARY_PATH
    """,
  "bat" :  #unchecked, but it should be something like this...
    """ 
    set PATH := {binDir};%PATH%
    """
}



import sys
if len(sys.argv) != 2:
  print "Usage: python createConfig.py <targetDirectory>"
  exit(1)

for filetype in setups:
  f = file ( "setup." + filetype , "w")
  f.write ( setups[filetype].format(binDir = sys.argv[1] ))
  f.close()


