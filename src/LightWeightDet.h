// Simple class to allow store a trained DET.
//
//
//
#ifndef LIGHTWEIGHTDET_H
#define LIGHTWEIGHTDET_H

#define DBG if (0) std::cout
#define FAILED_DECOMPRESSION 0xFA1DEC0
#define FAILED_READING       0xFA13EAD

#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <fstream>
#include <deque>
#include <map>
#include <functional>
#include <cstdio>
#include <sstream>
#include <cmath>

template <size_t N> 
class LightWeightDet
{
  public:
    typedef unsigned long int BinId;
////////////////////////////////////////////////////////////////////////////////
// Constructor
////////////////////////////////////////////////////////////////////////////////
    LightWeightDet() {m_bins.reserve(100);}
    LightWeightDet(const char* inputFileName);

////////////////////////////////////////////////////////////////////////////////
// Bin<N> structure                                                           //
////////////////////////////////////////////////////////////////////////////////
    template <size_t nVar>
    struct Bin
    {
      // default ctor
      Bin();

      /// Ctor
      Bin(std::vector<float>  _min, 
         std::vector<float>   _max,
         size_t               _binDepth,
         BinId                _highDaughterId,
         BinId                _lowDaughterId,
         float                _pdf    
        );

      /// Functions
      bool contains (const float* position) const;
      float getHyperVolume() const;
      std::string dump() const;

      /// Variables
      float min[N], max[N];
      size_t binDepth;
      BinId highDaughterId, lowDaughterId;
      float  pdf   ;
    }; //end struct Bin<N>

    struct SlimBin
    {
      SlimBin() {};
      SlimBin(int v, float th, float _pdf) 
        : varId(v), threshold(th), pdf(_pdf) {}

      int   varId;      ///< variable used to split this bin, -1 for leaves
      float threshold;  ///< threshold of the cut
      float pdf;        ///< pdf
    };

    Bin<N>* addBin (std::vector<float>  _min, 
                    std::vector<float>  _max,
                    size_t              _binDepth,
                    float               _pdf   
                   );

    // modify the tree structure
    bool createLink(const BinId parentBin, const BinId hSub, const BinId lSub);
    void splitBin (BinId bin, size_t iVar, float threshold);
    void prune (const float relativeDifferenceThreshold);
    void pruneDownToNbins( size_t targetN );
   

    // read the tree structure
    BinId getBinId (const float* position) const;
    float getBinMin (BinId id, size_t var) const;
    float getBinMax (BinId id, size_t var) const;
    size_t getNumberOfBins () const;

    // read the tree content 
    float eval (const float *position ) const;
    float slice_integrate (const float *min, const float *max) const;
    float integrate (const float *min, const float *max) const;
    float getNorm (void) const;

    float inverseIntegral(size_t varId, float intgr, const float *_min, const float *_max,
                          float precision = 1e-6) const;


    // output the tree to file (STL serialization)
    int writeToFile (const char *fileName);

    // updates the leaf list after modification to the tree structure
    void updateLeafList(void);

    
    
    ///////////////////////////////////////////////////////////////////////////
    // operators                                                             //
    ///////////////////////////////////////////////////////////////////////////
    LightWeightDet<N>& operator+= (const LightWeightDet<N>& rhs)
      { combineDets(rhs, [](float a, float b) {return a+b;});  return *this;}

    LightWeightDet<N>& operator-= (const LightWeightDet<N>& rhs)
      { combineDets(rhs, [](float a, float b) {return a-b;});  return *this;}

    LightWeightDet<N>& operator/= (const LightWeightDet<N>& rhs)
      { combineDets(rhs, [](float a, float b) {return a/b;});  return *this;}

    LightWeightDet<N>& operator*= (const LightWeightDet<N>& rhs)
      { combineDets(rhs, [](float a, float b) {return a*b;});  return *this;}

    LightWeightDet<N>& operator*= (const double n)
      { transform( [n](float x) {return n*x;}); return *this;}

    LightWeightDet<N>& operator/= (const double n)
      { transform( [n](float x) {return x/n;}); return *this;}

    LightWeightDet<N>& power_self (const double n)
      { transform( [n](float x) {return pow(x,n);}); return *this;}
  

  private:
    // service function for reading from file
    int readFromFile (const char* fileName);

    // takes the first element of the deque and remove it from the list 
    BinId popFirst (const float *min, const float *max, 
                      std::deque<BinId>& queue) const;


    // operated on trees
    void combineDets ( const LightWeightDet< N >& rhs, 
                       const std::function<float(float,float)>& combinationFunc);
    void transform (std::function<float(float)> func);

    void removeListedNodes (std::vector<BinId>&);

  public:
  LightWeightDet<N> & operator= (const LightWeightDet<N> rhs);

  private:
    std::vector < Bin<N> > m_bins;
    std::vector < BinId > m_leafList;

  public:
  LightWeightDet<N> operator* (float num)
    { LightWeightDet<N> ret(*this); ret *= num; return ret;}

  LightWeightDet<N> operator/ (float num)
    { LightWeightDet<N> ret(*this); ret /= num; return ret;}

  LightWeightDet<N> power (float num)
    { LightWeightDet<N> ret(*this); ret.power_self(num); return ret;}

  LightWeightDet<N> operator+ (LightWeightDet<N>& rhs)
    { LightWeightDet<N> ret (*this); ret += rhs; return ret; }


  LightWeightDet<N> operator- (LightWeightDet<N>& rhs)
    { LightWeightDet<N> ret (*this); ret -= rhs; return ret; }


  LightWeightDet<N> operator* (LightWeightDet<N>& rhs)
    { LightWeightDet<N> ret (*this); ret *= rhs; return ret; }


  LightWeightDet<N> operator/ (LightWeightDet<N>& rhs)
    { LightWeightDet<N> ret (*this); ret /= rhs; return ret; }

};


#include "src/LightWeightDet.icpp"


typedef LightWeightDet< 1> LwDet1 ;
typedef LightWeightDet< 2> LwDet2 ;
typedef LightWeightDet< 3> LwDet3 ;
typedef LightWeightDet< 4> LwDet4 ;
typedef LightWeightDet< 5> LwDet5 ;
typedef LightWeightDet< 6> LwDet6 ;
typedef LightWeightDet< 7> LwDet7 ;
typedef LightWeightDet< 8> LwDet8 ;
typedef LightWeightDet< 9> LwDet9 ;
typedef LightWeightDet<10> LwDet10;
typedef LightWeightDet<11> LwDet11;
typedef LightWeightDet<12> LwDet12;
typedef LightWeightDet<13> LwDet13;
typedef LightWeightDet<14> LwDet14;
typedef LightWeightDet<15> LwDet15;
typedef LightWeightDet<16> LwDet16;
typedef LightWeightDet<17> LwDet17;
typedef LightWeightDet<18> LwDet18;
typedef LightWeightDet<19> LwDet19;
typedef LightWeightDet<20> LwDet20;
typedef LightWeightDet<21> LwDet21;
typedef LightWeightDet<22> LwDet22;
typedef LightWeightDet<23> LwDet23;
typedef LightWeightDet<24> LwDet24;
typedef LightWeightDet<25> LwDet25;
typedef LightWeightDet<26> LwDet26;
typedef LightWeightDet<27> LwDet27;
typedef LightWeightDet<28> LwDet28;
typedef LightWeightDet<29> LwDet29;
typedef LightWeightDet<30> LwDet30;
#endif
