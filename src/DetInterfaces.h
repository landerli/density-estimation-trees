#ifndef _LIGHTDETWRITER_H_
#define _LIGHTDETWRITER_H_ 1

#include "LightWeightDet.h"
#include "RooDetPdfMultiCore.h"

#include "TH1.h"
#include "TH2.h"

typedef RooDetPdfMultiCore RooDetPdf;
typedef LwDet4 LwDetN;


template <int N>
class DetInterfaces 
{
  public:
    typedef std::vector<float> List;
    DetInterfaces () {};

    LightWeightDet<N>* makeLwDet (const RooDetPdf* pdf = NULL);

    void fillHistogram ( TH1* hist, LightWeightDet<N> *lwDet, size_t varId, 
                         const List* min = NULL, 
                         const List* max = NULL);

    void fillHistogram ( TH2* hist, 
                         LightWeightDet<N> *lwDet,
                         size_t var1Id,
                         size_t var2Id,
                         const List* min = NULL, 
                         const List* max = NULL);

    float integrate ( LightWeightDet<N> *lwDet, 
                         const List min, 
                         const List max) const;
};

#include "DetInterfaces.icpp"

#endif// _LIGHTDETWRITER_H_ 1
