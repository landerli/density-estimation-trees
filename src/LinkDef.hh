#ifdef __CLING__
#pragma link off all class;
#pragma link off all function;
#pragma link off all global;
#pragma link off all typedef;

#pragma link C++ class RooDetPdfMultiCore+;
#pragma link C++ class RooDetPdf+;
#pragma link C++ class LightWeightDet+;
#pragma link C++ class DetInterfaces+;
#endif

