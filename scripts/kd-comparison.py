################################################################################
## kd-comparison
##  uses the same dataset as the kd-example in Wikipedia to train a small DET
##  and show the difference
################################################################################

print "Importing libraries... "
import ROOT

## Setup the PyROOT environment
ROOT.gROOT.SetBatch()
## Defines the Density Estimation Trees library
ROOT.gSystem.Load ( "libDet" )

from ROOT import RooRealVar, RooDataSet, RooArgSet, RooArgList 
from ROOT import RooDetPdfMultiCore as RooDetPdf
from ROOT import DetInterfaces, TH2D, RooRandom

from DensityEstimationTrees import Report, Graphics, SampleGraph
from DensityEstimationTrees.RooFitCommands import *
print "Done."

# define axis variables
x = RooRealVar ( "x", "x", 0, 10 )
y = RooRealVar ( "y", "y", 0, 10 )

# define template histogram
h2_axes = TH2D ( "axes", ";x;y", 500, 0, 10, 500, 0, 10)

# Create a random dataset
variables = RooArgList ( x, y )


data = RooDataSet("data", "data", RooArgSet (x, y) )
def addPoint ( data, valX, valY ):
  x.setVal ( valX )
  y.setVal ( valY )
  data.add ( RooArgSet ( x, y ) )
  
addPoint ( data, 2, 3 )
addPoint ( data, 5, 4 )
addPoint ( data, 9, 6 )
addPoint ( data, 4, 7 )
addPoint ( data, 8, 1 )
addPoint ( data, 7, 2 )


# Train the RooDetPdf 
lww = DetInterfaces (2)()

## defines here the minimal bin width as total axis range / nBins
#x.setBins(15); y.setBins(15)
print "Training"
detPdf = RooDetPdf( 
        "det_wiki"        # name
      , "det_wiki"        # title
      , variables         # variables 
      , data              # inputtree
      , 1                 # min occupancy
      , RooDetPdf.kNone   # cross-valdation
      , RooDetPdf.kDepth  # complexity-function
      , 1                 # nThreads
  )
print "Converting"
det = lww.makeLwDet ( detPdf )
print "DET ready"

graph = SampleGraph ( 'x', 'y', data )


## Configure helper classes for setting style and create html report
stylist   = Graphics()
report    = Report ( "kd-comparison" )
report.outputTitle ( "Using the same sample as on Wikipedia (kd-tree)" )

canvas = ROOT.TCanvas ( "det", "det", 600, 600 )

## create the histogram
h2_det  = h2_axes.Clone("h2_det")
lww.fillHistogram ( h2_det, det, 0, 1 )

stylist.configAxes ( h2_det )

h2_det.Draw ( "col" )
graph.Draw ( "PSAME" )

report.outputCanvas ( "det" )

report.close()

