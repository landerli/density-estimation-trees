################################################################################
## examplePruning
##  small example to test pruning of LightWeight DETs
################################################################################

import ROOT

## Setup the PyROOT environment
ROOT.gROOT.SetBatch()
## Defines the Density Estimation Trees library
ROOT.gSystem.Load ( "libDet" )

from ROOT import RooDetPdfMultiCore as RooDetPdf
from ROOT import DetInterfaces, RooRandom, RooNDKeysPdf, RooArgList
from DensityEstimationTrees import Report, Graphics, SampleGraph
from DensityEstimationTrees.RooFitCommands import *
from DensityEstimationTrees import StandardDetForTests

## creates a standard det to be used for these tests
stdDet = StandardDetForTests(numEntries = 1000)

# Train the RooNDKeysPdf with the same data and variables
keys = RooNDKeysPdf ( "keys", "", stdDet.variableList, stdDet.data, "am", 1.4 ) 

## creates some shortcut to stdDet members
x = stdDet.x
y = stdDet.y

det = stdDet.LwDet
detPdf = stdDet.detPdf

## defines here the number of bins to be used in the plots
x.setBins(20); y.setBins(20)


## defines the helper class for ROOT interfaces
lww = DetInterfaces (2)()


### creates and fill histograms with the pdfs
h2_det  = stdDet.createHistogram ( "h2_det" )
h2_keys = stdDet.createHistogram ( "h2_keys" )

lww.fillHistogram ( h2_det, det, 0, 1 )
keys.fillHistogram ( h2_keys, RooArgList(x,y) )

## Configure helper classes for setting style and create html report
stylist   = Graphics()
report    = Report ( "examplePruning" )
report.outputTitle ( "Example of pruning" )

stylist.configAxes ( h2_det )
stylist.configAxes ( h2_keys )

graph = SampleGraph ( 'x', 'y', stdDet.data )

## Define graphical properties of the plots
min,max = (1e-7, .5e-4)
ROOT.gPad.SetLogz();
h2_det.SetMinimum ( min ); h2_det.SetMaximum ( max )
h2_det.Draw ( "colz" )
graph.Draw ( "PSAME" )

## save the plot to the html report
report.outputCanvas ( "det" )

h2_keys.SetMinimum ( min ); h2_keys.SetMaximum ( max )
h2_keys.Draw ( "colz" )
graph.Draw ( "PSAME" )
report.outputCanvas ( "keys" )

from copy import copy

oridet = copy(det)

################################################################################
## P R U N E   T H E   D E T  
################################################################################
det = copy ( oridet ) #restores the original copy
for nBins in [80]:
  det.pruneDownToNbins (nBins) #prune differences below 10%
  h2_det.Reset();
  lww.fillHistogram ( h2_det, det, 0, 1 )
  h2_det.Draw("colz");
  l = ROOT.TLatex(); l.SetNDC();
  l.DrawLatex (0.1, 0.1, str(det.getNumberOfBins()) + " bins") 
  report.outputCanvas ( "pruned_bin" + str(nBins).replace(".","p" ))




