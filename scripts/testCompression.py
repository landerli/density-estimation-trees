################################################################################
## simple2Dexample
##  used to display in an intuitive way what det does on a small sample
##  comparison with kernel density estimation is also provided
################################################################################

print "Importing libraries... "
import ROOT

## Setup the PyROOT environment
ROOT.gROOT.SetBatch()
## Defines the Density Estimation Trees library
ROOT.gSystem.Load ( "libDet" )

from ROOT import RooRealVar, RooProdPdf
from ROOT import RooDetPdfMultiCore as RooDetPdf
from ROOT import RooExponential, RooDataSet, RooArgSet
from ROOT import RooNDKeysPdf, RooArgList
from ROOT import DetInterfaces, TH2D
from ROOT import LightWeightDet

from DensityEstimationTrees import Report, Graphics
from DensityEstimationTrees.RooFitCommands import *
from ROOT import RooRandom
print "Done."

# define the random seed, this can be used to find 
# a configuration of the random dataset that better 
# displays some features of the algorithms
RooRandom.randomGenerator().SetSeed ( 3 )

# define axis variables
x = RooRealVar ( "x", "x", -1, 1 )
y = RooRealVar ( "y", "y", -1, 1 )

# define template histogram
h2_axes = TH2D ( "axes", ";x;y", 500, -1, 1, 500, -1, 1)

# define slope variables
cx = RooRealVar ( "cx", "cx", -3, -5, 0 )
cy = RooRealVar ( "cy", "cy", -3, -5, 0 )

# create target pdf
x_pdf = RooExponential ( "x_pdf", "x_pdf", x, cx )
y_pdf = RooExponential ( "y_pdf", "y_pdf", y, cy )

pdf = RooProdPdf ( "prodPdf", "", RooArgList ( x_pdf, y_pdf ) )

# Create a random dataset
variables = RooArgList ( x, y )

print "Generating dataset... "
data = pdf.generate ( RooArgSet(x,y), 50000 )
print "Done"

# Train the RooDetPdf 
lww = DetInterfaces (2)()

## defines here the minimal bin width as total axis range / nBins
#x.setBins(15); y.setBins(15)
print "Training"
detPdf = RooDetPdf( 
        "det_2exp"        # name
      , "det_2exp"        # title
      , variables         # variables 
      , data              # inputtree
      , 10                # min occupancy
      , RooDetPdf.kNone   # cross-valdation
      , RooDetPdf.kDepth  # complexity-function
      , 1                 # nThreads
  )
print "Converting"
det = lww.makeLwDet ( detPdf )
print "DET ready"

print "Writing compressed"
det.writeToFile ( "test-compressed.det" )
#print "Writing plain"
#det.writeToFile ( "test-plain.det" , False )
print "Done. "

reloadCompressed = LightWeightDet(2)( 'test-compressed.det')

print "Number of bins: {} {}".format(det.getNumberOfBins(), reloadCompressed.getNumberOfBins())
print "Integral:       {} {}".format(det.getNorm(), reloadCompressed.getNorm())

