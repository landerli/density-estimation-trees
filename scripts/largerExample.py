################################################################################
## largerExample
##  in this example I create efficiency tables and I explore the S/B ratio
##  comparison with kernel density estimation is also provided
################################################################################

print "Importing libraries... "
import ROOT

## Setup the PyROOT environment
ROOT.gROOT.SetBatch()
## Defines the Density Estimation Trees library
ROOT.gSystem.Load ( "libDet" )

from ROOT import RooRealVar, RooProdPdf
from ROOT import RooDetPdfMultiCore as RooDetPdf
from ROOT import RooExponential, RooDataSet, RooArgSet
from ROOT import RooNDKeysPdf, RooArgList, RooFormulaVar
from ROOT import RooGenericPdf, RooGaussian, RooAddPdf
from ROOT import DetInterfaces, TH2D
from ROOT import LightWeightDet

from DensityEstimationTrees import Report, Graphics
from DensityEstimationTrees.RooFitCommands import *
from ROOT import RooRandom
print "Done."

# define the random seed, this can be used to find 
# a configuration of the random dataset that better 
# displays some features of the algorithms
RooRandom.randomGenerator().SetSeed ( 3 )

# define axis variables
m  = RooRealVar ( "m",   "m",       0, 10 )
p  = RooRealVar ( "p",   "p",       0, 100 )
p1 = RooRealVar ( "p1",  "p_{1}",   0, 10  )
x  = RooRealVar ( "x",   "x",      -1, 1)


################################################################################
## Definition of the default mode
class mode:
  def __init__(s,name, pSlope, p1Frac, xMean, xSigma):
    s.name    = name
    s.p_slope = RooRealVar ( name+"_p_slope", "", pSlope ) 
    s.p1_frac = RooRealVar ( name+"_p1_frac", "", p1Frac )
    s.xm      = RooRealVar ( name+"_xm", "",      xMean  )
#    s.xs      = RooRealVar ( name+"_xs", "",      xSigma )
    s.xs      = RooFormulaVar ( name+"_xs", "", "{}/p**0.333".format(xSigma), 
                RooArgList(p) )
    s.p_distr = RooGenericPdf ( name +"_p_distr", "", 
      ( " (   p * exp(-p / {n}_p_slope) )"+
        " * ( p1 < {n}_p1_frac * p ? 1 : 0 ) *  ( p1 > 0.1 * {n}_p1_frac * p ? 1 : 0 )"+
        " * (exp ( -( x - {n}_xm )**2 / (2*{n}_xs**2 ) ) / ({n}_xs) ) "
      ) .format(n=name),
      RooArgList(p, p1, x, s.p_slope, s.p1_frac, s.xm, s.xs ) )

################################################################################
## Definition of the mass-peaking mode
class peakingMode (mode):
  def __init__ (s,name, pSlope, p1Frac, xMean, xSigma, mMean, mSigma):
    mode.__init__ (s, name, pSlope, p1Frac, xMean, xSigma )
    s.mm = RooRealVar ( name+"_mm", "", mMean )
    s.ms = RooRealVar ( name+"_ms", "", mSigma )
    s.m_distr = RooGaussian (name+ "_m_distr", "", m, s.mm, s.ms)

################################################################################
## Definition of the mass-peaking mode
class nonPeakingMode (mode):
  def __init__ (s,name, pSlope, p1Frac, xMean, xSigma, mSlope):
    mode.__init__ (s, name, pSlope, p1Frac, xMean, xSigma )
    s.mslope = RooRealVar ( name+"_mslope", "", mSlope )
    s.m_distr = RooExponential (name+ "_m_distr", "", m, s.mslope)


cfg_calib = mode("calib",         10, 0.15, 0.3, 0.6)
cfg_sig   = peakingMode("sig",     5, 0.15, 0.3, 0.6, 5, 0.6)
cfg_bkg   = nonPeakingMode("bkg",  2, 0.1, -0.3, 0.6, -0.2)

## Configure helper classes for setting style and create html report
stylist   = Graphics()
report    = Report ( "largerExample" )
report.outputTitle ( "Applications of Density Estimation Trees" )

sig_pdf = RooProdPdf("signal", "signal", RooArgList(cfg_sig.p_distr, cfg_sig.m_distr) )
bkg_pdf = RooProdPdf("background", "background", RooArgList(cfg_bkg.p_distr, cfg_bkg.m_distr) )

cf = RooRealVar ("cf", "cf", 0.15)
model = RooAddPdf("model", "model", sig_pdf, bkg_pdf, cf)

################################################################################
## Generating two random datasets

print 'Generating data set, please wait...'
data = model.generate ( RooArgSet(p, p1, x, m ), 100000)
print "Done."

print 'Generating calibration data set, please wait...'
calibration = cfg_calib.p_distr.generate ( RooArgSet(p, p1, x ), 1e6)
print "Done."

################################################################################
## Make several plots of the variables of the two datasets

for dataset in [data, calibration]:
  report << "Dataset: " << dataset.GetName() << '<br/>'
  varset = [m,p,p1,x] if dataset == data else [p,p1,x]
  for var in varset:
    report << "Variable: " << var.GetName() << '<br/>'
    frame = var.frame()
    dataset.plotOn ( frame )
    frame.Draw()
    report.outputCanvas ( dataset.GetName()  + "_" + var.GetName() )


  hist2d = dataset.createHistogram ( p, p1, 100, 100, "", "histogram" )
  hist2d.SetTitle (";p;p_{1}")
  hist2d.Draw("col")
  report.outputCanvas ( "p_p1_" + dataset.GetName() )

  report << "<br/>" 


# Train the RooDetPdf 
lww = DetInterfaces (3)()

p.setBins ( 30 ); p1.setBins ( 30 ); x.setBins ( 50 )
## defines here the minimal bin width as total axis range / nBins
#x.setBins(15); y.setBins(15)
print "Training"
detPdf = RooDetPdf( 
        "det_all"        # name
      , "det_all"        # title
      , RooArgList ( p,p1,x )
      , data              # inputtree
      , 10                # min occupancy
      , RooDetPdf.kNone   # cross-valdation
      , RooDetPdf.kDepth  # complexity-function
      , 1                 # nThreads
  )
print "Converting"
detAll = lww.makeLwDet ( detPdf )
print "DET ready"



################################################################################
## define a background-pure sample selecting sidebands
bkg_data = data.reduce ( ROOT.RooFit.Cut ( " m < 3  || m > 7 " ) )

################################################################################
## Create a RooDetPdf object using the sidebands
detBkgPdf = RooDetPdf( 
        "det_bkg"        # name
      , "det_bkg"        # title
      , RooArgList ( p,p1,x )
      , bkg_data          # inputtree
      , 10                # min occupancy
      , RooDetPdf.kNone   # cross-valdation
      , RooDetPdf.kDepth  # complexity-function
      , 1                 # nThreads
  )
print "Converting"
detBkg = lww.makeLwDet ( detBkgPdf )
print "DET ready"
      
################################################################################
## sets binning for drawing
p.setBins (100); p1.setBins (100); x.setBins (100)

################################################################################
## sets binning for drawing
frame = x.frame()
bkg_data.plotOn ( frame )
detBkgPdf.plotOn ( frame )
frame.Draw()

report.outputCanvas  ("det-training")


################################################################################
## BACKGROUND MINIMIZATION WITH CONSTANT OVERALL EFFICIENCY
##  this section defines a function fcn as a sort of constained chi2.
##  Minimizing this function one expects to have overall efficiency 
##  approximating 'targetEfficiency' including as little background as possible
################################################################################
from copy import copy
##     p     p1    x
min = ROOT.vector('float')()
max = ROOT.vector('float')()
targetEfficiency = 0.1
for i in [0,     0,   -1]: min.push_back(i)
for i in [100,  10,    1]: max.push_back(i)

def fcn ( var , par ):
  "chi2-like function used to determine the best configuration."
  cmin = copy (min)
  cmax = copy (max)
  
  if par[0] : cmin[0] = var[0]
  if par[1] : cmin[1] = var[1]
  if par[2] : cmin[2] = var[2]

  int_bkg = lww.integrate ( detBkg, cmin, cmax )
  int_all = lww.integrate ( detAll, cmin, cmax )

  nrm_bkg = detBkg.getNorm()
  nrm_all = detAll.getNorm()

  ret = (int_bkg/nrm_bkg)**2 + (int_all/nrm_all - targetEfficiency)**2 * 10

  return ret
  

### ROOT binder to minimizer for 3d functions
f = ROOT.TF3("f", fcn, min[0],max[0] , min[1],max[1], min[2],max[2], 3 )

### Parameters of the TF3 objects are boolean indicator of which 
###  variables are constants
f.SetParameters ( 1, 1, 1 )
minP  = ROOT.Double(0.)
minP1 = ROOT.Double(0.)
minX  = ROOT.Double(-1)


### For each set of parameters find the best configuration and produce a plot
for parset in [ lambda :  f.SetParameters ( 1, 1, 1 )
               , lambda :  f.SetParameters ( 1, 1, 0 )
              ] :
  parset()
  f.GetMinimumXYZ(minP, minP1, minX)
  print minP, minP1, minX

  selected_data = data.reduce ( "p > {minP} && p1 > {minP1} && x > {minX}"
                      .format ( minP = minP, minP1 = minP1, minX = minX ))

  frame = m.frame()
  selected_data.plotOn ( frame )
  frame.Draw()
  report.outputCanvas ( "plot" )


################################################################################
## Finalizing the html report (optional)
report.close()
