################################################################################
## simple2Dexample
##  used to display in an intuitive way what det does on a small sample
##  comparison with kernel density estimation is also provided
################################################################################

import ROOT

## Setup the PyROOT environment
ROOT.gROOT.SetBatch()
## Defines the Density Estimation Trees library
ROOT.gSystem.Load ( "libDet" )

from ROOT import RooRealVar, RooProdPdf
from ROOT import RooDetPdfMultiCore as RooDetPdf
from ROOT import RooExponential, RooDataSet, RooArgSet
from ROOT import RooNDKeysPdf, RooArgList
from ROOT import DetInterfaces, TH2D

from DensityEstimationTrees import Report, Graphics
from DensityEstimationTrees.RooFitCommands import *
from ROOT import RooRandom

# define the random seed, this can be used to find 
# a configuration of the random dataset that better 
# displays some features of the algorithms
RooRandom.randomGenerator().SetSeed ( 3 )

# define axis variables
x = RooRealVar ( "x", "x", -1, 1 )
y = RooRealVar ( "y", "y", -1, 1 )

# define template histogram
h2_axes = TH2D ( "axes", ";x;y", 500, -1, 1, 500, -1, 1)

# define slope variables
cx = RooRealVar ( "cx", "cx", -3, -5, 0 )
cy = RooRealVar ( "cy", "cy", -3, -5, 0 )

# create target pdf
x_pdf = RooExponential ( "x_pdf", "x_pdf", x, cx )
y_pdf = RooExponential ( "y_pdf", "y_pdf", y, cy )

pdf = RooProdPdf ( "prodPdf", "", RooArgList ( x_pdf, y_pdf ) )

# Create a random dataset
variables = RooArgList ( x, y )
data = pdf.generate ( RooArgSet(x,y), 1000 )

# Train the RooNDKeysPdf
keys = RooNDKeysPdf ( "keys", "", RooArgList ( x, y), data, "am", 1.4 ) 

# Train the RooDetPdf 
lww = DetInterfaces (2)()

## defines here the minimal bin width as total axis range / nBins
x.setBins(15); y.setBins(15)
detPdf = RooDetPdf( 
        "det_2exp"        # name
      , "det_2exp"        # title
      , variables         # variables 
      , data              # inputtree
      , 10                # min occupancy
      , RooDetPdf.kNone   # cross-valdation
      , RooDetPdf.kDepth  # complexity-function
      , 1                 # nThreads
  )
det = lww.makeLwDet ( detPdf )

## defines here the number of bins to be used in the plots
x.setBins(20); y.setBins(20)


## creates a graph with markers corresponding to the entries 
## randomly generated
### define buffer vectors
vector = ROOT.vector('double')
x_data = vector()
y_data = vector()

### loops over entries, fill buffer vectors
for i in range(0, data.numEntries()):
  d = data.get(i)
  x_data.push_back ( d.find('x').getVal() )
  y_data.push_back ( d.find('y').getVal() )

### creates the graph
graph = ROOT.TGraph ( x_data.size(), x_data.data(), y_data.data())

### creates and fill histograms with the pdfs
h2_det  = h2_axes.Clone("h2_det")
h2_keys = h2_axes.Clone("h2_keys")
lww.fillHistogram ( h2_det, det, 0, 1 )
keys.fillHistogram ( h2_keys, RooArgList(x,y) )

## Configure helper classes for setting style and create html report
stylist   = Graphics()
report    = Report ( "simple2Dexample" )
report.outputTitle ( "Simple 2D example" )

stylist.configAxes ( h2_det )
stylist.configAxes ( h2_keys )

## Define graphical properties of the plots
min,max = (1e-7, .5e-4)
ROOT.gPad.SetLogz();
h2_det.SetMinimum ( min ); h2_det.SetMaximum ( max )
h2_det.Draw ( "colz" )
graph.SetMarkerSize(2)
graph.SetMarkerStyle(20)
graph.Draw ( "PSAME" )

## save the plot to the html report
report.outputCanvas ( "det" )

h2_keys.SetMinimum ( min ); h2_keys.SetMaximum ( max )
h2_keys.Draw ( "colz" )
graph.Draw ( "PSAME" )
report.outputCanvas ( "keys" )

ROOT.gPad.SetLogz(False);
ROOT.gPad.SetLogy(True);
## Iterate on a set of smearing values plotting the projections each time.
for smearing in [0, 0.05, 0.1, 0.2]:
  detPdf.setSmearingWidth("x", smearing)
  detPdf.setSmearingWidth("y", smearing)
  for var in [x,y]:
    frame = var.frame()
    data.plotOn(frame)
    detPdf.plotOn(frame)
    keys.plotOn(frame, LineColor(ROOT.kRed), LineStyle(9) )
    data.plotOn(frame)
    frame.Draw()
    report.outputCanvas("var" + var.GetTitle() + "_smear" + str(smearing))


