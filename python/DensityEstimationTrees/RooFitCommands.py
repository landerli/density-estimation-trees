import ROOT

LineColor        = ROOT.RooFit.LineColor
LineStyle        = ROOT.RooFit.LineStyle  
MarkerSize       = ROOT.RooFit.MarkerSize 
MarkerColor      = ROOT.RooFit.MarkerColor
MarkerStyle      = ROOT.RooFit.MarkerStyle
DataError        = ROOT.RooFit.DataError  
LineWidth        = ROOT.RooFit.LineWidth  
Components       = ROOT.RooFit.Components 

