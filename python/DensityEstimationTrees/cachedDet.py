from ROOT import LightWeightDet
def cachedDet (nVars, cacheFile, helperFunction):
  ret = LightWeightDet(nVars) ( cacheFile )
  if ret.getNumberOfBins() == 0:
    ret = helperFunction()
    ret.writeToFile ( cacheFile )

  return ret
  
