import ROOT

class SampleGraph:
  def __init__ (self, varx, vary, roodata
      , markerSize = 1
      , markerStyle = 20 
      , markerColor = ROOT.kBlack
    ):
    ## creates a graph with markers corresponding to the entries 
    ## randomly generated
    ### define buffer vectors
    vector = ROOT.vector('double')
    x_data = vector()
    y_data = vector()

    ### loops over entries, fill buffer vectors
    for i in range(0, roodata.numEntries()):
      d = roodata.get(i)
      x_data.push_back ( d.find('x').getVal() )
      y_data.push_back ( d.find('y').getVal() )

    ### creates the graph
    self.graph = ROOT.TGraph ( x_data.size(), x_data.data(), y_data.data())
    self.graph.SetMarkerSize  ( markerSize  )
    self.graph.SetMarkerStyle ( markerStyle )
    self.graph.SetMarkerColor ( markerColor )


  def Draw (self, options):
    self.graph.Draw ( options )



