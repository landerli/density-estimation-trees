################################################################################
## StandardDetForTests
##   To avoid repeating the construction of a density estimation tree for each
##   example, here there is a routine producing a simple DET usable for many
##   examples.
################################################################################
import ROOT
ROOT.gSystem.Load ( "build/libDet.so" )

from ROOT import TH2D
from ROOT import RooRealVar, RooProdPdf
from ROOT import RooDetPdfMultiCore as RooDetPdf
from ROOT import RooGaussian, RooDataSet, RooArgSet
from ROOT import RooNDKeysPdf, RooArgList
from ROOT import RooAddPdf
from ROOT import DetInterfaces
from ROOT import RooRandom
from DensityEstimationTrees.RooFitCommands import *

class Gaus2D:
  "Helper class to create a 2D Gaussian distribution"
  def __init__ ( self, name, x, y, x0, y0, xw, yw ):
    # define mean variables
    self.x0 = RooRealVar ( name+"x0", "x0", x0, -.5, .5 )
    self.y0 = RooRealVar ( name+"y0", "y0", y0, -.5, .5 )

    #define width variables
    self.xw = RooRealVar ( name+"xw", "xw", xw, 0.01, 1 )
    self.yw = RooRealVar ( name+"yw", "yw", yw, 0.01, 1 )

    # create target pdf
    self.x_pdf = RooGaussian ( name+"x_pdf", "x_pdf", x, self.x0, self.xw )
    self.y_pdf = RooGaussian ( name+"y_pdf", "y_pdf", y, self.y0, self.yw )

    self.pdf = RooProdPdf ( name+ "prodPdf", "", 
                            RooArgList ( self.x_pdf, self.y_pdf ) )



class StandardDetForTests:
  "Define a helper class to create a 2d Gaussian pdf in 1 line"

  def __init__ ( self , seed = 1, numEntries = 1000, minOccupancy = 10, nBins = 15):

    # define the random seed, this can be used to find 
    # a configuration of the random dataset that better 
    # displays some features of the algorithms
    RooRandom.randomGenerator().SetSeed ( seed )

    # define axis variables
    self.x = RooRealVar ( "x", "x", -1, 1 )
    self.y = RooRealVar ( "y", "y", -1, 1 )

    # define template histogram
    self.h2_axes = TH2D ( "axes", ";x;y", 500, -1, 1, 500, -1, 1)


    self.cf = RooRealVar ("cf", "cf", 0.4, 0, 1 )
    self.gaus1 = Gaus2D ( "g1" , self.x, self.y, -0.5, -0.5, 0.2, 0.2 )
    self.gaus2 = Gaus2D ( "g2" , self.x, self.y, +0.5, +0.5, 0.2, 0.2 )
    self.pdf = RooAddPdf ( "twoGaus", "", self.gaus1.pdf, self.gaus2.pdf, self.cf)

    # Create a random dataset
    self.variableList = RooArgList ( self.x, self.y )
    self.variableSet  = RooArgSet  ( self.x, self.y )
    self.data = self.pdf.generate ( RooArgSet(self.x,self.y), numEntries )


    # Train the RooDetPdf 
    lww = DetInterfaces (2)()

    ## defines here the minimal bin width as total axis range / nBins
    self.x.setBins(nBins); self.y.setBins(nBins)
    self.detPdf = RooDetPdf( 
            "det_2exp"            # name
          , "det_2exp"            # title
          , self.variableList     # variables 
          , self.data             # inputtree
          , minOccupancy          # min occupancy
          , RooDetPdf.kNone       # cross-valdation
          , RooDetPdf.kDepth      # complexity-function
          , 1                     # nThreads
      )
    self.LwDet = lww.makeLwDet ( self.detPdf )

  def createHistogram ( self, name ):
    return self.h2_axes.Clone ( name )



