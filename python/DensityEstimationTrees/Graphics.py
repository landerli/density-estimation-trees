import ROOT

class Graphics:
  def __init__ ( self, style = None ):
    self._style = style
    self.LabelSize    = 0.05
    self.LabelOffset  = 0.01
    self.TitleSize    = 0.05
    self.TitleOffset  = 0.05
    self.Font         = 132
    self.MarkerStyle  = 20
    self.MarkerSize   = 0.05

    if style == '':
      pass

    ROOT.gStyle.SetOptStat  ( 0 )
    ROOT.gStyle.SetOptTitle ( 0 )


  def configAxes ( self, hist ):
    for axis in [hist.GetXaxis(), hist.GetYaxis()]:
      axis.SetLabelSize   ( self.LabelSize )
      axis.SetLabelOffset ( self.LabelOffset )
      axis.SetLabelFont   ( self.Font )
      axis.SetTitleSize   ( self.TitleSize )
      axis.SetTitleSize   ( self.TitleOffset )
      axis.SetTitleFont   ( self.Font )

  def configAsFilled ( self, hist, color ):
    hist.SetFillStyle ( 1000 )
    hist.SetFillColor ( color )
    hist.SetLineWidth ( 0 )

  def configAsErrBars ( self, hist, color ):
    hist.SetFillStyle ( 0 )
    hist.SetLineColor ( color )
    hist.SetMarkerStyle ( self.MarkerStyle )
    hist.SetMarkerColor ( color )
    hist.SetMarkerSize  ( self.MarkerSize )

