from ROOT import gPad, TCanvas


class Report:
  def __init__( self
                , filename
                , path = './www/'
                , options = ''
                , enabled = True
              ):
    """
    Constructor for Report class.

    Arguments:
     . filename, name of the output file report
     . path, direcotory where the file is saved
     . options (string):
        > saveMacro
    """
    self._filename       = filename
    self._path           = path
    self._options        = options
    self._figIndex       = 0
    self._enabled        = enabled

    self._myCanvas = TCanvas(filename, filename, int(1.618033*600), 600)
    self._myCanvas.SetLeftMargin   (0.16) 
    self._myCanvas.SetRightMargin  (0.05) 
    self._myCanvas.SetTopMargin    (0.05) 
    self._myCanvas.SetBottomMargin (0.20) 

    self._myCanvas.cd()

    if not self._enabled: return
    
    self._file = open(path+'/'+filename+".html", 'w')
    self._file.write("""
      <HTML>
        <HEAD>
          <TITLE>"""+filename+"""</TITLE>
        </HEAD>
        <BODY>
    """)


  def __lshift__(self, text):
    if not self._enabled: return
    self._file.write(str(text))
    return self

  def outputTitle(self, title):
    if not self._enabled: return
    self._file.write("<H1>"+title+"</H1>")


  def _figureFileName(self, name, extension):
    if not self._enabled: return
    return "img/Fig-" + self._filename + "_"  \
            + str(self._figIndex) + "_" + name + "." + extension

  def outputCanvas(self, name, options="width=45%"):
    "Saves the current Canvas to pdf and png files, and includes them in report"
    if not self._enabled: return

    # Saves the files
    gPad.SaveAs(self._path + '/' + self._figureFileName(name, "pdf"))
    gPad.SaveAs(self._path + '/' + self._figureFileName(name, "png"))

    #logs the output in report
    self << "<A border=0 href=\"" + self._figureFileName(name, "pdf") + "\">" \
         << "<IMG src=\"" + self._figureFileName(name, 'png') + "\""+options\
         << "/></A>\n";

    self._figIndex += 1
  
  def close(self):
    if not self._enabled: return
    "Close the report output file"
    self._file.write("</BODY></HTML>")
    self._file.close()

  def printDict(self, inputDict, options = None):
    if not self._enabled: return
    if not isinstance(inputDict, dict):
      print "HTML::Report::printDict was given not a dictionary"
      return

    if options == None: options = ""
    
    self << "<TABLE>{}\n".format(options)
    for entry in inputDict:
      self << "<TR><TD>" << str(entry) << "<TD>" << str(inputDict[entry])<<"\n"
    self << "</TABLE>\n\n"

  def enable(): 
    self._enabled = True
  def disable():
    self._enabled = False


  #internal variables
  _filename = None
  _path     = None
  _options  = None
  _file     = None
  _figIndex = None
  _myCanvas = None
  _enabled  = True


  br = "<BR/>\n"

