from BaseTest import BaseTest

class test_pruning (BaseTest):
  'Pruning'
  def test(self): 
    for nb in [100, 80, 70, 30, 10, 1]:
      self.lwDet.pruneDownToNbins ( nb )
      if self.lwDet.getNumberOfBins() > nb:
        return "pruning failed on request of "+str(nb)+" bins. " + str(self.lwDet.getNumberOfBins())

    return 0

