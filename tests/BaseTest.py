import ROOT
ROOT.gROOT.SetBatch()

from ROOT import gSystem
gSystem.Load ( "libDet" )

from ROOT import RooRealVar, RooProdPdf
from ROOT import RooDetPdfMultiCore as RooDetPdf
from ROOT import RooExponential, RooDataSet, RooArgSet
from ROOT import RooNDKeysPdf, RooArgList
from ROOT import DetInterfaces, TH2D

from DensityEstimationTrees import Report, Graphics
from DensityEstimationTrees.RooFitCommands import *
from ROOT import RooRandom


class BaseTest:
  def __init__(self,  nEntries = 1000 ):
    # define the random seed, this can be used to find 
    # a configuration of the random dataset that better 
    # displays some features of the algorithms
    RooRandom.randomGenerator().SetSeed ( 3 )

    # define axis variables
    self.x = RooRealVar ( "x", "x", -1, 1 )
    self.y = RooRealVar ( "y", "y", -1, 1 )

    # define slope variables
    cx = RooRealVar ( "cx", "cx", -3, -5, 0 )
    cy = RooRealVar ( "cy", "cy", -3, -5, 0 )

    # create target pdf
    x_pdf = RooExponential ( "x_pdf", "x_pdf", self.x, cx )
    y_pdf = RooExponential ( "y_pdf", "y_pdf", self.y, cy )

    pdf = RooProdPdf ( "prodPdf", "", RooArgList ( x_pdf, y_pdf ) )

    # Create a random dataset
    variables = RooArgList ( self.x, self.y )
    data = pdf.generate ( RooArgSet(self.x,self.y), 1000 )

    # Train the RooDetPdf 
    lww = DetInterfaces (2)()

    ## defines here the minimal bin width as total axis range / nBins
    self.x.setBins(15); self.y.setBins(15)
    self.detPdf = RooDetPdf( 
            "det_2exp"        # name
          , "det_2exp"        # title
          , variables         # variables 
          , data              # inputtree
          , 10                # min occupancy
          , RooDetPdf.kNone   # cross-valdation
          , RooDetPdf.kDepth  # complexity-function
          , 1                 # nThreads
      )
    self.lwDet = lww.makeLwDet ( self.detPdf )

  def test(self):
    "test function that has to be overridden returning 0 on success."
    print "not overridden"
    return 1



