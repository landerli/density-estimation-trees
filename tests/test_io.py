import ROOT
from BaseTest import BaseTest

class test_io (BaseTest):
  'FileInputOutput'
  def test(self):
    
    self.lwDet.writeToFile("testdet.det")

    loadedDet = ROOT.LwDet2( "testdet.det" )

    nBinsLwDet  = self.lwDet.getNumberOfBins()
    nBinsLoaded = loadedDet. getNumberOfBins()

    if (nBinsLwDet != nBinsLoaded):
      return "RooDetPdf has " + str(nBinsLoaded) + " bins, while LwDet: " + str ( nBinsLwDet )

    normLoaded = loadedDet.getNorm() 
    normOrig   = self.lwDet.getNorm()
  
    if (normLoaded != normOrig): 
      return "different norms: Loaded/Written",  normOrig , "/", normOrig
      
    return 0

