from BaseTest import BaseTest

class test_conversion (BaseTest):
  'Conversion RooDetPdf -> LwDet'
  def test(self):
    nBinsLwDet = self.lwDet.getNumberOfBins()
    nBinsPdf   = self.detPdf.getNbins()
    if (nBinsLwDet != nBinsPdf):
      return "RooDetPdf has " + str(nBinsPdf) + " bins, while LwDet: " + str ( nBinsLwDet )
    return 0


