import tests
import sys
from copy import copy

def color_bad ( text ):
  return '\033[91m' + str(text) + '\033[0m'

def color_ok  ( text ):
  return '\033[92m' + str(text) + '\033[0m'

if __name__ == '__main__':
  for var in tests.__dict__:
    if 'test_' in var:
      module = tests.__dict__[var].__dict__[var]
      if module.__doc__ == None or module.__doc__ == "": continue;

      testName  = module.__doc__
      test      = module
      sys.stdout.write ( "Testing " + testName + "... " )

      result = test().test()

      if result == 0:
        print color_ok("PASSED.")
      else:
        print color_bad("ERROR ") + result
  
